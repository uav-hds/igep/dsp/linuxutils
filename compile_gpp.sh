unset LDFLAGS

function green_echo ()
{
    echo -e "\033[32m$1\033[0m"
}

function red_echo ()
{
    echo -e "\033[31m$1\033[0m"
}

function check_error ()
{
    if [ "$?" != "0" ]; then
        red_echo "Erreur, arret"
        exit 1
    fi
}


green_echo "Compilation cmem"
cd $TI_TOOLS_BASE_DIR
cd linuxutils/packages/ti/sdo/linuxutils/cmem
make clean
make release > /dev/null
check_error
cp src/interface/release/cmem.o470MV src/interface/release/cmem.o
#cp src/module/cmemk.ko $IGEP_ROOT/uav_dev/bin/arm
#cp -R include $IGEP_ROOT/uav_dev/include/cmem
#cp src/interface/release/cmem.o $IGEP_ROOT/uav_dev/lib/arm
